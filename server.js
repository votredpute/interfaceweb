var express = require('express');
var helmet = require('helmet');
var app = express();

var MongoClient = require("mongodb").MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://votredepute:inf6150@ds139899.mlab.com:39899/heroku_fpw34v7h'

app.use(helmet.noCache()); // Seulement le temps que l'on débogue
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');


// Page "accueil""
app.get('/', function(req, res) {
    res.render('pages/index');
});

// Page "à notre sujet"
app.get('/a-notre-sujet', function(req, res) {
    res.render('pages/a-notre-sujet');
});

//Page statistiques
app.get('/statistiques', function(req, res) {
    res.render('pages/statistiques');
});
//page liste votes-projet
app.get('/projet-loi', function(req, res) {
    var resultArray = [];
    MongoClient.connect(url, function(err, db){
        assert.equal(null,err);
        var cursor = db.collection('projetsloi').find();
        cursor.forEach(function(doc,err) {
            assert.equal(null,err);
            resultArray.push(doc);
        }, function(){
            db.close();
            res.render('pages/projet-loi', {votes: resultArray});
        });
    });
});
// Page "liste des députés"
app.get('/deputes', function(req, res) {
    var resultArray = [];
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        var cursor = db.collection('deputes').find();
        cursor.forEach(function(doc, err) {
            assert.equal(null,err);
            resultArray.push(doc);
        }, function() {
            db.close();
            res.render('pages/deputes', {politiciens: resultArray});
        });
    });
});

app.get('/loi/id/:id' , function(req,res) {
    var loiId = parseInt(req.params.id);
    MongoClient.connect(url, function(err , db) {
        assert.equal(null, err);
        db.collection('projetsloi').findOne(
            {"legisinfo_id": loiId},
            function(err, loiTrouve) {
               res.render('pages/loi', {loi: loiTrouve}); 
            });
    });
});

// Page "profil complet d'un député"
app.get('/depute-profil-infos/id/:id', function(req, res) {
    var deputeId = parseInt(req.params.id);
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        db.collection('deputes').findOne(
            { "memberships.end_date": null, "memberships.riding.id": deputeId },

            function(err, deputeTrouve) {
                res.render('pages/depute-profil-infos', {depute: deputeTrouve});
            }
        );
    });
});

// Page "profil complet d'un député"
app.get('/depute-profil-depenses/id/:id', function(req, res) {
    var deputeId = parseInt(req.params.id);

    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        db.collection('deputes').findOne(
            {"memberships.riding.id": deputeId }, {expenditures:1},

            function(err, deputeTrouve) {
                //res.render('pages/depute-profil-depenses', {depute: deputeTrouve});
                res.send(deputeTrouve);
            }
        );
    });
});

// Page "Recherche"
app.get('/rechercher', function(req, res) {
    var item = req.query.recherche;
    var tabItem = [];
    MongoClient.connect(url, function(err , db) {
        assert.equal(null, err);
        var cursor = db.collection('deputes').find({"name": "/.*"+item+".*/"});
        cursor.forEach(function(doc, err) {
            assert.equal(null,err);
            console.log(doc);
            tabItem.push(doc);
        });
        
    });
});


app.listen(8080);
console.log('Running on port 8080...');
